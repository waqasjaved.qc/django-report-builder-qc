var reportBuilder = angular.module('reportBuilder', ['ngRoute', 'restangular', 'ngMaterial', 'ui.tree', 'ngHandsontable', 'angularPikaday']);

reportBuilder.config(function($mdThemingProvider) {
    $mdThemingProvider.definePalette('CorporatePalette', {
        '50': '115d98',
        '100': '115d98',
        '200': '115d98',
        '300': '115d98',
        '400': '115d98',
        '500': '0a6aaf',
        '600': '0a6aaf',
        '700': '0a6aaf',
        '800': '0a6aaf',
        '900': '0a6aaf',
        'A100': '115d98',
        'A200': '0a6aaf',
        'A400': '0a6aaf',
        'A700': '0a6aaf',
        'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
                                            // on this palette should be dark or light
        'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
            '200', '300', '400', 'A100'],
        'contrastLightColors': ['500', '600', //hues which contrast should be 'dark' by default
            '700', '800', '900', 'A700']
    });
    $mdThemingProvider.theme('default')
        .primaryPalette('CorporatePalette')
        .accentPalette('orange')
});

reportBuilder.config(function($sceProvider) {
   $sceProvider.enabled(false);
});

reportBuilder.config(function(RestangularProvider) {
    RestangularProvider.setBaseUrl(BASE_URL + "api");
    RestangularProvider.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
      var extractedData;
      if (operation === "getList" && _.has(data, 'meta')) {
        extractedData = data.data;
        extractedData.meta = data.meta;
      } else {
        extractedData = data;
      }
      return extractedData;
    });
    return RestangularProvider.setRequestSuffix("/");
});

function static(path) {
    /* Works like django static files - adds the static path */
    return STATIC_URL + path;
}

reportBuilder.config(function($routeProvider, $httpProvider, $locationProvider) {
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $routeProvider.
        when("/", {
            controller: "homeCtrl",
            templateUrl: static('report_builder/partials/home.html')
        }).
        when("/report/add", {
            controller: "addCtrl",
            templateUrl: static('report_builder/partials/add.html')
        }).
        when("/report/:reportId", {
            controller: "homeCtrl",
            templateUrl: static('report_builder/partials/home.html')
        })
    return $locationProvider.html5Mode(true);
});

reportBuilder.run(['$route', '$rootScope', '$location', function ($route, $rootScope, $location) {
    var original = $location.path;
    $location.path = function (path, reload) {
        if (reload === false) {
            var lastRoute = $route.current;
            var un = $rootScope.$on('$locationChangeSuccess', function () {
                $route.current = lastRoute;
                un();
            });
        }
        return original.apply($location, [path]);
    };
    $rootScope.MEDIA_URL = MEDIA_URL;
    $rootScope.STATIC_URL = STATIC_URL;
}]);

var reportBuilderApp = angular.module('reportBuilderApp', ['reportBuilder']);
